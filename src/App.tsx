import { Routes, Route } from "react-router-dom"
import BlogPage from "./pages/BlogPage/BlogPage";
import Home from "./pages/Home/Home";
import AllBlogs from "./pages/AllBlogs/AllBlogs";
import PrivacyPolicy from "./pages/PrivacyPolicy/PrivacyPolicy";
import TermsAndConditions from "./pages/TermsAndConditions/TermsAndConditions";



function App() {

  return (  
    <>
    <Routes>
        <Route path="/" element={ <Home/> } />
        <Route path="/:slug/:id" element={ <BlogPage/> } />
        <Route path="/all-blogs" element={ <AllBlogs/> } />
        <Route path="/privacy-policy" element={ <PrivacyPolicy/> } />
        <Route path="/terms-and-conditions" element={ <TermsAndConditions/> } />
      </Routes>
    </>

  )
}

export default App
