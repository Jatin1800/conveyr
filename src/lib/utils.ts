import { type ClassValue, clsx } from "clsx"
import { twMerge } from "tailwind-merge"
import { wpBaseUrl } from "./constants/constants";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}


export const formatDate = (dateString: string | Date): string => {
  const date = new Date(dateString);
  const day = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const year = date.getFullYear();
  return `${day}/${month}/${year}`;
};

export function scrollToSection(sectionId:string) {
  const element = document.querySelector(sectionId);
  if (element) {
    element.scrollIntoView({ behavior: 'smooth' });
    // window.scrollTo({
    //   top: 300,
    //   left: 0,
    //   behavior: "smooth",
    // })
  }
}

export async function fetchImage(id:number, type:string="medium"){

  try {
    const response = await fetch(`${wpBaseUrl}wp-json/wp/v2/media/${id}`)
    const blog = await response.json()
      
        const url =  blog?.media_details?.sizes[type]?.source_url
        return url
     
     
  } catch (error) {
    console.error("Unable to fetch thumbnail: ", error)
  }
 
}