

export const featuresList=[
    {
        title:"C-Chime",
        desc:"make your post chime, photos glitter, and liked GIFs, videos, live videos, audio etc. Voice your opinion on hot topic related to politics, economy and many other topics.",
        icon:"Chime",
    },
    {
        title:"C-Blog",
        desc:"You have something and you can contribute to society with valuable comments and educate with your Blog. Join millions here and connect.",
        icon:"Blog",
    },
    {
        title:"C-Chat",
        desc:"Connect with your loved ones without any worries. Save your family members in a category and you don’t have to run the home page to find your family member.",
        icon:"Chat",
    },
    {
        title:"Video Chat-Live Streaming",
        desc:"Have a video chat, voice call or live streaming (to be added) with your office friends save as in a group or a category.",
        icon:"VideoChat",
    },
    {
        title:"Get help from admin",
        desc:"The user can directly chat with admin and get relevent information or help from him/her.",
        icon:"Admin",
    },
    {
        title:"24x7 assistance",
        desc:"Making app more reliable with providing 24x7 support to the end user.",
        icon:"Assistance",
    },
]