export const blogCardDummyData = [
  {
    imgSrc: "./blog-img.png",
    category: ["React", "Development"],
    title: "How to Handle a Social Media Crisis as a Content Creator",
    desc: "The bigger you grow on social media, the more likely you are to run into a situation that requires deft handling. Here's how to tackle that",
    blogLink: "/blog",
  },
  {
    imgSrc: "./blog-img.png",
    category: ["React", "Development"],
    title: "How to Handle a Social Media Crisis as a Content Creator",
    desc: "The bigger you grow on social media, the more likely you are to run into a situation that requires deft handling. Here's how to tackle that",
    blogLink: "/blog",
  },
  {
    imgSrc: "./blog-img.png",
    category: ["React", "Development"],
    title: "How to Handle a Social Media Crisis as a Content Creator",
    desc: "The bigger you grow on social media, the more likely you are to run into a situation that requires deft handling. Here's how to tackle that",
    blogLink: "/blog",
  },
  {
    imgSrc: "./blog-img.png",
    category: ["React", "Development"],
    title: "How to Handle a Social Media Crisis as a Content Creator",
    desc: "The bigger you grow on social media, the more likely you are to run into a situation that requires deft handling. Here's how to tackle that",
    blogLink: "/blog",
  },
];
