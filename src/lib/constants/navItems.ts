export const NavItems = [
    {
        name:"Home",
        link:"#home"
    },
    {
        name:"Features",
        link:"#features"
    },
    {
        name:"Pricing",
        link:"#pricing"
    },
    {
        name:"Blogs",
        link:"#blog"
    },
    {
        name:"Contact",
        link:"#contact"
    }
]