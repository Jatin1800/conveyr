import { lazy, useEffect, Suspense } from "react"
import Hero from "@/layout/Hero/Hero"
import Stats from "@/layout/Stats/Stats"
import Features from "@/layout/Features/Features"
import SectionImg from "@/assets/images/Frame 3920.png"
// import PricingSection from "@/layout/Pricing/PricingSection"
const BlogsSection = lazy(()=>import("@/layout/BlogsSection/BlogsSection"))
import Newsletter from "@/layout/Newsletter/Newsletter"
import ContactForm from "@/layout/ContactForm/ContactForm"
import { scrollToSection } from "@/lib/utils"
import VideoSection from "@/layout/VideoSection/VideoSection"
import MediaGallery from "@/layout/MediaGallery/MediaGallery"

const Home = () => {
  useEffect(() => {
    const hash = window.location.hash;
    if(hash && hash.length>0) scrollToSection(hash)
  }, []);

  return (
    <>
    <Hero/>
     <Stats/>
     <Features/>
     <VideoSection/>
     <img src={SectionImg} alt="Feature Image" />
     {/* <PricingSection/> */}
     <Suspense fallback={"Loading"}>
      <MediaGallery/>
     <BlogsSection/>
     </Suspense>
     <Newsletter/>
     <ContactForm/>
     </>
  )
}

export default Home