import styles from "./blogPage.module.css";
// import BlogImg from "@/assets/images/blog-title-img.png";
// import AuthorImg from "@/assets/images/author.png";
import BlogCard from "@/components/blogCard/BlogCard";
import parse from "html-react-parser";

import { wpBaseUrl, wpUserPostsDomain } from "@/lib/constants/constants";
import { useParams, useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { Blog } from "@/types";
import { fetchImage, formatDate } from "@/lib/utils";

const BlogPage = () => {
  const { id } = useParams();
  const { pathname } = useLocation();
  const [blogContent, setBlogContent] = useState<Blog | null>(null);
  const [imgUrl, setImgUrl] = useState<string>("")

  const [blogs, setBlogs] = useState<[Blog] | []>([]);

  async function fetchBlogs() {
    const response = await fetch(`${wpBaseUrl}${wpUserPostsDomain}`);
    let blogs = await response.json();
    // console.warn("blogs ==== ", blogs);
    if (blogs?.length >= 4) {
      setBlogs(blogs?.splice(0, 4));
    } else {
      setBlogs(blogs);
    }
  }

  async function fetchCurrentBlog() {
    const response = await fetch(`${wpBaseUrl}${wpUserPostsDomain}/${id}`,  {cache: "no-store"});
    
    let blog = await response.json();
    setBlogContent(blog);
    
    const url = await fetchImage(blog.featured_media,"full")
    console.warn("blogs ==== ", url);
      setImgUrl(url)

  }

  useEffect(() => {
    fetchCurrentBlog();
    fetchBlogs();
    
  }, [id]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);
  return (
    <>
      <div className={styles.header}></div>
      <div className={styles.main}>
        <div className={styles.content}>
          <div className={styles.left}>
            <div className={styles.headerImg}>
              <img
                src={
                  imgUrl
                }
                alt=""
              />
              <div className={styles.headerImgText}>
                <div className={styles.headerImgTextBlur}></div>
                <div className={styles.headerImgDetails}>
                  {/* <div className={styles.headerImgCat}>
                    <span></span>
                    {blogContent &&
                    blogContent?.tags.map(
                        (category, id) => <p key={id}>{category}</p>
                      )}
                  </div> */}
                  <h1>{blogContent && parse(blogContent?.title?.rendered)}</h1>
                  <div className={styles.headerImgDate}>
                    <span>{blogContent && formatDate(blogContent?.date)}</span>
                    {/* •<span>10 min read</span> */}
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.mainContent}>
              <h2 className="mb-8 text-white">
                {" "}
                {blogContent && parse(blogContent?.title?.rendered)}
              </h2>
              <div className={styles.mainContentText}>
                {blogContent && parse(blogContent.content.rendered)}
              </div>
            </div>
          </div>
          {/* <div className={styles.right}>
            <div className={styles.author}>
              <p className="font-semibold">Author:</p>
              <div className={styles.authorImg}>
                <img
                  src={blogContent ? blogContent.author.avatar_URL : AuthorImg}
                  alt="author Image"
                />
                <img src={LinkedIn} alt="LinkedIn logo" /> 

                <h3>{blogContent && blogContent.author.name}</h3>
              </div>
              <p>
                Founder of SAAS First - the Best AI and Data-Driven Customer
                Engagement Tool
              </p>
              <span></span>
              <p>
                With 11 years in SaaS, I've built MillionVerifier and SAAS
                First. Passionate about SaaS, data, and AI. Let's connect if you
                share the same drive for success!
              </p>
            </div>
            <div className={styles.article}>
                <h3>In this article</h3>
                <div>Exploring Generative AI in Content Creation</div>
                <div>Steering Clear of Common AI Writing Pitfalls</div>
                <div>Understanding ChatGPT Capabilities - Define Your Style</div>
                <div>Understand Your Readers</div>
                <div>Creating Quality AI-powered Blogs that Stand Out</div>
                <div>Conclusion: Embracing AI in Blog Creation</div>
                <div>Afterword: The AI Behind This Article</div>
            </div> 
          </div> */}
        </div>
        <div className={styles.relatedBlogs}>
          <h3>Related Blogs</h3>
          <div className={styles.blogsSectionCards}>
            {blogs.length > 0 ? (
              blogs.map((blog) => (
                <BlogCard
                  key={blog.id}
                  img={{ mediaId: blog.featured_media, type: "thumbnail" }} //send image type as large, thumbnail, medium, medium_large,
                  title={blog?.title?.rendered}
                  desc={blog.excerpt.rendered}
                  // category={Object.keys(blog.categories)}
                  blogLInk={blog.slug}
                  id={blog.id}
                />
              ))
            ) : (
              <h4 className="text-5xl text-[#8d4ce5] font-medium mx-[auto] mb-12">
                No Blogs Found!!
              </h4>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default BlogPage;
