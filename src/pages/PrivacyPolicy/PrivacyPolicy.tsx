
const PrivacyPolicy = () => {
  return (
    <div className="p-12 text-2xl">
      <p>PRIVACY POLICY&nbsp;</p>
      <p
        style={{
          marginTop: "12.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <strong>Algebracorp (OPC) Private Limited</strong>
        <strong>&nbsp;</strong>(the<strong>&nbsp;“Company”</strong>) owns{" "}
        <span style={{ color: "black", background: "white" }}>
          and operates the interactive website{" "}
          <strong>
            <a href="http://www.conveyr.in">www.conveyr.in</a>
          </strong>
        </span>
        <span style={{ color: "black" }}>
          &nbsp;its mobile-friendly website interface and{" "}
          <strong>Conveyr</strong>
          &nbsp;
        </span>
        <span style={{ color: "black" }}>
          its mobile application, available in both Android and iOS
          (collectively and individually referred to as the “
          <strong>Platform</strong>”). The Platform provides&nbsp;
        </span>
        <strong>
          <span style={{ color: "black" }}>
            Micro Blogging, Blogging &amp; Chat
          </span>
        </strong>
        <strong>
          <span style={{ color: "black" }}>&nbsp;</span>
        </strong>
        <span style={{ color: "black" }}>
          (“<strong>Service</strong>”).{" "}
          <span style={{ background: "white" }}>
            We want you to know that your privacy is important to us and to
            protect the Personal Information (defined below)&nbsp;
          </span>
          of any person who registers on or accesses the Platform for
          informational purposes,{" "}
          <span style={{ background: "white" }}>
            we hereby provide this notice explaining our privacy practices for
            the collection, storage, usage, and protection of your Data (defined
            below).
          </span>
        </span>
      </p>
      <p
        style={{
          marginTop: "12.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <em>
          <span style={{ color: "black" }}>
            Kindly note, under this Privacy Policy (the “<strong>Policy</strong>
            ”), the use of terms like “<strong>we</strong>”, “
            <strong>us</strong>” and “<strong>ours</strong>” refers to the
            Company and the terms “<strong>you</strong>”, “<strong>your</strong>
            ”, “<strong>yours</strong>” and “<strong>user</strong>” refers to
            any natural or legal person who browses through the Platform or
            avails the Services.
          </span>
        </em>
      </p>
      <p
        style={{
          marginTop: "12.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        The procedure for handling and securing the Personal Information shall
        be governed by this Policy, which is published and shall be construed in
        accordance{" "}
        <span style={{ color: "black", background: "white" }}>
          with the provisions of the Information Technology Act 2000,
          Information Technology (Reasonable security practices and procedures
          and sensitive personal data or information) Rules, 2011 and the
          applicable legislation relating to the privacy or data protection in
          other jurisdictions (the “
          <strong>
            <em>Regulations</em>
          </strong>
          ”).&nbsp;
        </span>
      </p>
      <p
        style={{
          marginTop: "12.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black", background: "white" }}>
          By accessing the Platform, you hereby acknowledge{" "}
          <span style={{ border: "none windowtext 1.0pt", padding: "0cm" }}>
            that you have read and understood the terms of this Policy and agree
            to be bound by them. However, if you disagree, kindly exit the
            Platform immediately as your continued access shall be construed as
            an acceptance of this Policy.
          </span>
        </span>
      </p>
      <ol className="decimal_type" style={{ listStyleType: "decimal" }}>
        <li>
          <strong>
            <span style={{}}>DEFINITIONS</span>
          </strong>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            Under this Policy, the below-mentioned terms shall have the same
            meaning as assigned to them:
          </span>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Applicable Law(S)&nbsp;
            </span>
          </strong>
          <span style={{ fontFamily: "Calibri" }}>shall&nbsp;</span>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            mean&nbsp;
          </span>
          <span style={{ color: "black" }}>and refer to&nbsp;</span>
          <span style={{ background: "white" }}>
            all the applicable provisions of the prevailing laws, statutes,
            common law, regulations, ordinances, codes, rules, guidelines,
            orders, permits, licenses, tariffs, and approvals relating to this
            Policy.
          </span>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Cookie(S)&nbsp;
            </span>
          </strong>
          <span style={{ fontFamily: "Calibri" }}>
            is a small piece of data/software code
            <strong>
              <span style={{ color: "black" }}>&nbsp;</span>
            </strong>
            <span style={{ color: "black", background: "white" }}>
              tracked automatically and stored by your web browser or on the
              hard drive of your device. Cookies allow the Platform to remember
              your actions/information and preferences over time and generally
              help in improving your Platform experience.
            </span>
          </span>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>Data&nbsp;</span>
          </strong>
          <span style={{}}>
            shall refer to the Non-Personal and Personal Information,
            individually and in combination.
          </span>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>
              Non – Personal data&nbsp;
            </span>
          </strong>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            is anonymized data incapable of identifying an individual. It refers
            to any other information, not covered under Personal Information,
            which may be collected by us when you access or use the Platform and
            includes information about your device, Internet Protocol (IP)
            address, operating system, browser type and version
          </span>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            , geographical location, URLs of referring/exit pages, pages
            visited, device ID, amongst others
            <span style={{ background: "white" }}>.</span>
          </span>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>Personal data</span>
          </strong>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            &nbsp;means any information that relates to a natural person, which,
            either{" "}
            <span style={{ background: "white" }}>
              directly or indirectly, in combination with other information
              available or likely to be available with a body corporate, is
              capable of identifying such person. Personal Information refers to
              the name, contact details, identification number, location data,
              data stored in the devices, or factors specific to the physical,
              physiological, genetic, mental, economic, cultural, or social
              identity of that User. Any reference to Personal Information shall
              include Sensitive Personal Data.
            </span>
          </span>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Sensitive Personal Data&nbsp;
            </span>
          </strong>
          <span style={{ fontFamily: "Calibri" }}>
            shall have the same meaning as provided under <em>Section 3</em> of
            the&nbsp;
          </span>
          <span style={{ fontFamily: "Calibri" }}>
            Information Technology (Reasonable security practices and procedures
            and sensitive personal data or information) Rules, 2011.
          </span>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Services&nbsp;
            </span>
          </strong>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            shall mean&nbsp;
          </span>
          <strong>
            <span style={{ color: "black" }}>
              Micro Blogging, Blogging &amp; Chat
            </span>
          </strong>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              User&nbsp;
            </span>
          </strong>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            means and refers to every individual or legal entity that registers
            itself on the Platform for availing the Services.
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span
          style={{
            border: "none windowtext 1.0pt",
            padding: "0cm",
            background: "white",
          }}
        >
          &nbsp;
        </span>
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{}}>INTERPRETATION</span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            Unless otherwise defined, the context used in this Policy denotes:
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            Whenever the context so requires, “you”, “your” and “user” shall
            mean any natural or legal person who browses through the Platform
            and the terms “we”, “us” and “our” shall refer to the Company.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            Heading or bold typeface is used only for convenience and shall be
            ignored for interpretation.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            Words using the singular or plural number also include the plural
            and singular, respectively.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            Wherever in the Agreement, words are used in the masculine gender,
            they shall be read and construed in the feminine gender also,
            whenever it should apply.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            The terms hereof, hereby, hereto, and derivative or similar words
            refer to this entire Policy or specified clauses of the Policy, as
            the case may be.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            Reference to the word “include” shall be construed as without
            limitation.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            Reference to any legislation or Applicable Law or any provision
            thereof shall refer to any such applicable legislation, law, or
            provision as amended, suspended, or re-enacted from time to time.
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              APPLICABILITY
            </span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            This Policy applies to any person who visits the Platform to gain
            information about the Company or the Services provided by the
            Company; intends to avail the Services or has certain queries
            regarding the Services and usage of the Platform. You hereby agree
            not to provide the Personal Data of any other person to us.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            This Policy governs the access, collection, usage, handling,
            storage, and disclosure of such Personal Data in accordance with the
            terms as given below:
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>&nbsp;</span>
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              COLLECTION OF DATA
            </span>
          </strong>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Personal Data
            </span>
          </strong>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>
          While using our Service, we may ask you to provide us with certain
          personally identifiable information that can be used to contact or
          identify you ("
        </span>
        <strong>
          <span style={{ color: "black" }}>Personal Data</span>
        </strong>
        <span style={{ color: "black" }}>
          "). Personally, identifiable information may include but is not
          limited to:&nbsp;
        </span>
      </p>
      <ul style={{ listStyleType: "disc", marginLeft: 44 }}>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            First Name and last name
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            Email address &nbsp;
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            Phone number&nbsp;
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            Address, State, Province, ZIP/Postal code, City
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            Contact logbook saved on the Mobile device&nbsp;
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            Cookies and Usage Data&nbsp;
          </span>
        </li>
      </ul>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>
          We collect personally identifiable information that you may
          voluntarily provide on online forms, which may include: user
          registration, contact requests, guest comments, online surveys, and
          other online activities. We may use your Personal Data to contact you
          with newsletters, marketing or promotional materials, and other
          information that may be of interest to you. You may opt-out of
          receiving any, or all, of these communications from us by following
          the unsubscribe link or the instructions provided in any email we
          send.
        </span>
      </p>
      <ol style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Usage Data
            </span>
          </strong>
          <span style={{ fontFamily: "Calibri", color: "black" }}>&nbsp;</span>
        </li>
      </ol>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>
          When you access the Service with a mobile device, we may collect
          certain information automatically, including, but not limited to, the
          type of mobile device you use, your mobile device unique ID, the IP
          address of your mobile device, your mobile operating system, the type
          of mobile internet browser you use, unique device identifiers and
          other diagnostic data ("
        </span>
        <strong>
          <span style={{ color: "black" }}>Usage Data</span>
        </strong>
        <span style={{ color: "black" }}>").&nbsp;</span>
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>&nbsp;</span>
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>&nbsp;</span>
      </p>
      <ol style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Location Data
            </span>
          </strong>
          <span style={{ color: "black" }}>&nbsp;</span>
        </li>
      </ol>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>
          We may use and store information about your location if you give us
          permission to do so ("
        </span>
        <strong>
          <span style={{ color: "black" }}>Location Data</span>
        </strong>
        <span style={{ color: "black" }}>
          "). We use this data to provide features of our Service, to improve
          and customize our Service. You can enable or disable location services
          when you use our Service at any time by way of your device
          settings.&nbsp;
        </span>
      </p>
      <ol style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Tracking &amp; Cookies Data
            </span>
          </strong>
          <span style={{ fontFamily: "Calibri", color: "black" }}>&nbsp;</span>
        </li>
      </ol>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>
          We use cookies and similar tracking technologies to track the activity
          on our Service and we hold certain information.&nbsp;
        </span>
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>
          Cookies are files with a small amount of data which may include an
          anonymous unique identifier. Cookies are sent to your browser from the
          Platform and stored on your device. Other tracking technologies are
          also used such as beacons, tags, and scripts to collect and track
          information and to improve and analyze our Service.&nbsp;
        </span>
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>
          You can instruct your browser to refuse all cookies or to indicate
          when a cookie is being sent. However, if you do not accept cookies,
          you may not be able to use some portions of our Service.&nbsp;
        </span>
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "1.0cm",
          textAlign: "justify",
          textIndent: "7.65pt",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>
          Examples of Cookies we use:&nbsp;
        </span>
      </p>
      <ul style={{ listStyleType: "disc", marginLeft: 44 }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Session Cookies:
            </span>
          </strong>
          <span style={{ color: "black" }}>
            &nbsp;We use Session Cookies to operate our Service.&nbsp;
          </span>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Preference Cookies:
            </span>
          </strong>
          <span style={{ color: "black" }}>
            &nbsp;We use Preference Cookies to remember your preferences and
            various settings.&nbsp;
          </span>
        </li>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri", color: "black" }}>
              Security Cookies:
            </span>
          </strong>
          <span style={{ color: "black" }}>
            &nbsp;We use Security Cookies for security purposes.
          </span>
        </li>
      </ul>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "54.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>&nbsp;</span>
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span
              style={{
                fontFamily: "Calibri",
                color: "black",
                background: "white",
              }}
            >
              USE OF DATA
            </span>
          </strong>
        </li>
        <li>
          <span style={{}}>
            We use information that we collect about you or that you provide to
            us, including any personal information:&nbsp;
          </span>
        </li>
      </ol>
      <ul style={{ listStyleType: "disc", marginLeft: 44 }}>
        <li>
          <span style={{}}>
            To present our Platform and its contents to you.
          </span>
        </li>
        <li>
          <span style={{}}>
            To create your profile on our Platform and provide you with
            information, products, or services that you request from us.
          </span>
        </li>
        <li>
          <span style={{}}>To&nbsp;</span>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            administer contests, promotions, surveys, or other site features.
          </span>
        </li>
        <li>
          <span style={{}}>
            To sync the contacts of your mobile device to enable you to
            establish a network with your contacts over our platform and enable
            the chatting feature.
          </span>
        </li>
        <li>
          <span style={{}}>
            To send invite links to those contacts who are not present on our
            platform, thereby increasing your contact circle over our
            platform.&nbsp;
          </span>
        </li>
        <li>
          <span style={{}}>
            To fulfill any other purpose for which you provide it.&nbsp;
          </span>
        </li>
        <li>
          <span style={{}}>
            To provide you with notices about your account.&nbsp;
          </span>
        </li>
        <li>
          <span style={{}}>
            To carry out our obligations and enforce our rights arising from any
            contracts entered into between you and us, including for billing and
            collection.&nbsp;
          </span>
        </li>
        <li>
          <span style={{}}>
            To notify you about changes to our Platform or any products or
            services we offer or provide through it.&nbsp;
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To notify you in the event we become aware of any breach of the
            security of your information and take appropriate actions to the
            best of our ability to remedy such a breach.&nbsp;
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To enable us to comply with the legal and regulatory obligations and
            to notify you regarding your account, to troubleshoot problems with
            your account, to resolve a dispute, to collect fees or monies owed,
            to confirm your identity in order to ensure that you are eligible to
            use this Platform.
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To enable us to undertake the activity of data analytics on the
            information shared and the pattern of usage in order to make the
            decision of providing you with the content of your interest or with
            the content in which you might be interested to enable you to use
            our services to the fullest.&nbsp;
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To ask for ratings and reviews of services or products.
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To follow up with them after correspondence (live chat, email, or
            phone inquiries).
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To send periodic emails
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To conduct research and analysis to detect, prevent, mitigate and
            investigate fraudulent or illegal activities;&nbsp;
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To monitor aggregate metrics such as total number of viewers,
            visitors, traffic, and demographic patterns;&nbsp;
          </span>
        </li>
        <li>
          <span style={{}}>
            To allow you to participate in interactive features on our
            Platform.&nbsp;
          </span>
        </li>
        <li>
          <span style={{}}>
            In any other way, we may describe when you provide the
            information.&nbsp;
          </span>
        </li>
        <li>
          <span style={{}}>For any other purpose with your consent.&nbsp;</span>
        </li>
      </ul>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span
              style={{
                fontFamily: "Calibri",
                color: "black",
                background: "white",
              }}
            >
              DISCLOSURE AND SHARING OF DATA
            </span>
          </strong>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            We may disclose aggregated information about our users, and
            information that does not identify any individual, without
            restriction.&nbsp;
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            We may disclose personal information that we collect, or you provide
            as described in this privacy policy:&nbsp;
          </span>
        </li>
      </ol>
      <ul
        className="decimal_type"
        style={{ listStyleType: "disc", marginLeft: 44 }}
      >
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To our subsidiaries and affiliates.&nbsp;
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To contractors, service providers, and other third parties&nbsp;
          </span>

          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            we use to support our business.
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To a buyer or other successor in the event of a merger, divestiture,
            restructuring, reorganization, dissolution, or other sale or
            transfer of some or all our assets, whether as a going concern or as
            part of bankruptcy, liquidation, or similar proceeding, in which
            personal information held by us about our Platform users is among
            the assets transferred.
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To third parties to market their products or services to you.&nbsp;
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            To fulfill the purpose for which it has been provided to us.&nbsp;
          </span>
        </li>
        <li>
          <span
            style={{
              fontFamily: "Calibri",
              color: "black",
              background: "white",
            }}
          >
            For any other purpose disclosed by us when you provide the
            information with your consent.&nbsp;
          </span>
        </li>
      </ul>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            We will disclose your Data in good faith if such action is
            necessary: to comply with the legal obligation; to protect and
            defend the rights and property and restrict the legal liability of
            the Company; to prevent or investigate possible wrongdoing in
            connection with the Platform, or to protect the personal safety of
            other Users of the Platform.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            We may disclose your information where such disclosure is necessary
            or required (i) by law or regulation, for complying with legal
            process or government requests (including in response to public
            authorities to meet national security or law enforcement
            requirements), or (ii) to exercise, establish or defend our legal
            rights.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            For any other purpose, we may disclose your Personal Information
            with your consent.
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span
          style={{
            border: "none windowtext 1.0pt",
            padding: "0cm",
            background: "white",
          }}
        >
          &nbsp;
        </span>
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>DATA RETENTION</span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri", color: "black" }}>
            We may retain your data for <strong>10&nbsp;</strong>years or for
            such period as may be required under the Applicable law and use your
            Personal Information as and when necessary to comply with our legal
            obligations or to resolve disputes. Consistent with these
            requirements, we will try to delete your Personal Information within
            the time limits imposed under the Applicable Law(s) or upon request,
            if possible. When we have no justifiable business need to process
            your Personal Information, we will either delete or anonymize it,
            or, if this is not possible (for example, because your Personal
            Information has been stored in backup archives), then we will
            securely store your Personal Information and isolate it from any
            further processing until the deletion is possible.
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black", background: "white" }}>&nbsp;</span>
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>DATA SECURITY</span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: '"Times New Roman"', color: "black" }}>
            We secure information you provide on computer servers in a
            controlled, secure environment, protected from unauthorized access,
            use, or disclosure. We maintain reasonable administrative,
            technical, and physical safeguards in an effort to protect against
            unauthorized access, use, modification, and disclosure of Personal
            Information in its control and custody. All the details are
            encrypted and stored in our cloud database. All files are password
            protected with us. However, no data transmission over the Internet
            or wireless network can be guaranteed. Therefore, while we strive to
            protect your Personal Information, you acknowledge that (i) there
            are security and privacy limitations of the Internet which are
            beyond our control; (ii) the security, integrity, and privacy of any
            and all information and data exchanged between you and our Platform
            cannot be guaranteed; and (iii) any such information and data may be
            viewed or tampered with in transit by a third party, despite best
            efforts.
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>&nbsp;</span>
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>
              THIRD-PARTY LINKS AND CONTENTS THEREOF
            </span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            The Platform may contain links to third-party websites and the same
            shall not constitute, in any manner whatsoever the Company’s
            endorsement, sponsorship, or a recommendation of such third-party
            websites or their products, services, content, and offerings. The
            Company is not responsible for examining or evaluating any
            third-party links and does not make any representation or warranty
            for their products, services, content, and offerings or their terms
            of use and privacy practices. In the event a User accesses such
            websites, he shall be under the obligation to apprise him of their
            terms and conditions and privacy practices before providing any
            personal or sensitive information. Any transaction with such
            websites shall be at the User’s own risk and expense.
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <strong>&nbsp;</strong>
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>
              SERVICE PROVIDERS&nbsp;
            </span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            We may employ third-party companies and individuals to facilitate
            our Service <strong>("Service Providers")</strong>, to provide the
            Service on our behalf, to perform Service-related services or to
            assist us in analysing how our Service is used.&nbsp;
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        These third parties have access to your Personal Data only to perform
        these tasks on our behalf and are obligated not to disclose or use it
        for any other purpose.&nbsp;
      </p>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <strong>Analytics:</strong> We may use third-party Service Providers to
        monitor and analyse the use of our Service. Google Analytics Google
        Analytics is a web analytics service offered by Google that tracks and
        reports website traffic. Google uses the data collected to track and
        monitor the use of our Service. This data is shared with other Google
        services. Google may use the collected data to contextualize and
        personalize the ads of its own advertising network. You can opt-out of
        having made your activity on the Service available to Google Analytics
        by installing the Google Analytics opt-out browser add-on. The add-on
        prevents the Google Analytics JavaScript (ga.js, analytics.js, and
        dc.js) from sharing information with Google Analytics about visits
        activity. For more information on the privacy practices of Google,
        please visit the Google Privacy Policy and Terms of Use web page:
        https://play.google.com/about/developer-content-policy/
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>DISCLAIMER</span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            We collect the Data only when an individual or the authorized
            representative of a business entity, as the case may be, shares such
            Data on our Platform. We may further verify and collate the data by
            using the information about such individual or business entity
            available in the public domain, as permitted by law, to prevent
            cases of identity theft, fraud, cybercrimes, etc.
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>
              LIABILITY AND WARRANTY
            </span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            Although we have implemented and employed the necessary internet
            security methods and technology to secure the Data transmitted to
            us; however, the security of the Data transmitted over the internet
            cannot be guaranteed; thereby we cannot ensure or warrant the
            security of any information that you transmit to us. You acknowledge
            and agree that you share the Personal Information with us entirely
            at your own risk. In light of the above, we declare that:
          </span>
        </li>
      </ol>
      <ul style={{ listStyleType: "disc", marginLeft: 44 }}>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            We shall not be held liable for any loss or injury caused to you, as
            a result of voluntary disclosure of the Data by you to a third
            party.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            We are liable only for any recommendations made by our authorized
            person through this Platform.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            Notwithstanding anything contained in this Policy, we disclaim all
            the warranty for any loss, damage, or misuse of the Data.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            The use of any product by you other than recommended by us through
            this Platform/call centre of the Company will be at your own cost
            and responsibility.&nbsp;
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            You expressly agree that in the event you post information, you will
            be deemed to have given authority to the other users to act
            accordingly and we are not liable for the same.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            In the event, you post any photo, content, or any other Personal
            Information or Non-Personal Information through this Platform, the
            same will be treated as granting of a non-exclusive, transferable,
            sub-licensable, royalty-free worldwide access to use such
            information by you to us.&nbsp;
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            We take no responsibility or liability for the privacy practices and
            security of Data collected by the third-party website linked to our
            Platform as they are not within the ambit of our control.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            By accepting these terms of use, you agree to relieve us from any
            liability whatsoever arising from your use of information from any
            third party, or your use of any third-party website, or your use of
            any products purchased from us.&nbsp;
          </span>
        </li>
      </ul>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "54.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>ELIGIBILITY</span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            The usage of our Platform is fit for individuals from all age
            groups. However, for availing of the Service, you should be at least
            18 (Eighteen) years of age.&nbsp;
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            If you use the services on behalf of an individual or an entity, you
            represent that you are authorized by such individual or entity to
            (i) accept this privacy policy on their behalf, and (ii) consent on
            their behalf to our collection, use and disclosure of the Data as
            described in this Policy.
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <ol style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>
              SEVERABILITY AND EXCLUSION&nbsp;
            </span>
          </strong>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        We have taken every effort to ensure that this Policy adheres to the
        applicable laws. The invalidity or unenforceability of any part of this
        Policy shall not prejudice or affect the validity or enforceability of
        the remainder of this Policy. This Policy does not apply to any
        information other than the information collected by the Company through
        this Platform. This Policy shall be inapplicable to any unsolicited
        information you provide us through this Platform or through any other
        means. This includes, but is not limited to, information posted in any
        public areas of this Platform. All unsolicited information shall be
        deemed to be non-confidential and the Company shall be free to use and/
        or disclose such unsolicited information without any limitations.
      </p>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <ol style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>NO WAIVER</span>
          </strong>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        The rights and remedies available under this Policy may be exercised as
        often as necessary and are cumulative and not exclusive of rights or
        remedies provided by the law. It may be waived only in writing. Delay in
        exercising or non-exercise of any such right or remedy does not
        constitute a waiver of that right or remedy, or any other right or
        remedy.
      </p>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <ol className="decimal_type" style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>
              CONDITIONS OF USE AND AMENDMENTS
            </span>
          </strong>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            This Policy should be read at all times along with the Terms of Use
            published by the Company on the Platform.
          </span>
        </li>
        <li>
          <span style={{ fontFamily: "Calibri" }}>
            The Policy may be amended{" "}
            <span style={{ color: "black", background: "white" }}>
              in response to changing legal, technical, and business
              developments or&nbsp;
            </span>
            at the sole discretion of the Company. Unless otherwise specified,
            such revisions shall take effect from the day they are posted on the
            Platform. You are advised to consult this Policy regularly for any
            changes, and your continued access and Platform usage shall be
            deemed as an acceptance of the changes in the Policy.
          </span>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <ol style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>
              JURISDICTION OF LAW:&nbsp;
            </span>
          </strong>
        </li>
      </ol>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        In case of any dispute regarding the privacy policy of
        <strong>&nbsp;Algebracorp (OPC) Private Limited,</strong> an arbitrator
        shall be appointed by the Company to ascertain the rights and
        liabilities of the parties. You further accept that all disputes are
        subject to laws applicable in the India<strong>&nbsp;</strong>and
        subject to jurisdictions of courts in the ­­­­­­­­­­
        <strong>New Delhi</strong> only.
      </p>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <ol style={{ listStyleType: "undefined" }}>
        <li>
          <strong>
            <span style={{ fontFamily: "Calibri" }}>GRIEVANCE REDRESSAL</span>
          </strong>
        </li>
      </ol>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        In the event of any queries or grievances regarding our Services or if
        you wish to report a security breach or otherwise require any
        assistance, you may contact our Grievance Redressal Officer Mr./Ms.
        <strong>Manas Srivastava</strong> at E-mail Address:&nbsp;
        <a href="mailto:customerservice@conveyr.in">
          customerservice@conveyr.in
        </a>
        <strong>.</strong>
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <strong>&nbsp;</strong>
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <strong>&nbsp;</strong>
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <strong>&nbsp;</strong>
      </p>
      <p>
        <strong>
          <span
            style={{
              fontSize: 35,
              lineHeight: "107%",
              fontFamily: '"Freestyle Script"',
              color: "#7030A0",
            }}
          >
            Conveyr Management
          </span>
        </strong>
      </p>
      <p>
        <strong>
          <span
            style={{
              fontSize: 35,
              lineHeight: "107%",
              fontFamily: '"Freestyle Script"',
              color: "#7030A0",
            }}
          >
            Conveyr - A product of Algebracorp (OPC) Private Limited
          </span>
        </strong>
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "36.0pt",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <p
        style={{
          marginTop: "0cm",
          marginRight: "0cm",
          marginBottom: "0cm",
          marginLeft: "42.55pt",
          textIndent: "-42.55pt",
          lineHeight: "150%",
        }}
      >
        &nbsp;
      </p>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span style={{ color: "black" }}>&nbsp;</span>
      </p>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span
          style={{
            border: "none windowtext 1.0pt",
            padding: "0cm",
            background: "white",
          }}
        >
          &nbsp;
        </span>
      </p>
      <p
        style={{
          marginBottom: "0cm",
          textAlign: "justify",
          lineHeight: "150%",
        }}
      >
        <span
          style={{
            border: "none windowtext 1.0pt",
            padding: "0cm",
            background: "white",
          }}
        >
          &nbsp;
        </span>
      </p>
   
    </div>
  );
};

export default PrivacyPolicy;
