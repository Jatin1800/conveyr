import { useEffect, useState } from "react";
import Logo from "@/assets/images/logo.png";
import { Button } from "@/components/ui/button";
import { NavItems } from "@/lib/constants/navItems";
import styles from "./navbar.module.css";
import { Link } from "react-router-dom";
import { scrollToSection } from "@/lib/utils";
const Navbar = () => {
  const [showBackground, setShowBackground] = useState(false);
  const TOP_OFFSET = 50;

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY >= TOP_OFFSET) {
        setShowBackground(true);
      } else {
        setShowBackground(false);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div className={`${styles.navbar} ${showBackground && styles.navScroll} `}>
      <Link to="/" className="w-1/2 h-full pl-[7rem] bg-[#8e4be9]" >
        <div className={styles.logo}>
          <img src={Logo} alt="Logo" />
          <span className="font-medium text-[2rem] md:text-[3.3rem] leading-[4.6rem] text-left">
            Conveyr
          </span>
        </div>
      </Link>
      <div className="w-1/2 h-full flex items-center gap-8 md:gap-12 text-xl leading-[2.5rem] font-medium justify-evenly bg-[#8e4be9] pr-[7rem]">
        <ul className="flex items-center gap-8 md:gap-12 justify-evenly">
          {NavItems.map((item) => (
            <li key={item.name} className="text-white">
              <Link to={`/${item.link}`}>
                <span onClick={() => scrollToSection(item.link)}>
                  {item.name}
                </span>
              </Link>
            </li>
          ))}
        </ul>
        <Link to="/#app-download">
          <Button
            className="rounded-[40px] text-xl px-[1.6rem] py-[2.7rem] "
            onClick={() => scrollToSection("#app-download")}
          >
            Download now!
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default Navbar;
