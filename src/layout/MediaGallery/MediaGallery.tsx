import  { useEffect, useState } from "react";
import styles from "./mediaGallery.module.css";
import VideoSlider from "@/components/videoSlider/VideoSlider";
import { wpUserMediaDomain, wpBaseUrl } from "@/lib/constants/constants";

const MediaGallery = () => {
  const [videos, setVideos] = useState([]);


  useEffect(() => {
    try {
      // Fetch video media from WordPress REST API
      fetch(`${wpBaseUrl}${wpUserMediaDomain}`, {cache: "no-store"})
        .then((res) => res.json())
        .then((data) =>setVideos( data.filter((file:any)=> file.mime_type === "video/mp4")));
    } catch (error: any) {
      console.error("Error fetching videos:", error);
    }
  }, []);

  if (videos.length > 0 ) return (
    <div className={styles.main}>
      <h2 className={styles.title}>Media Gallery</h2>
      <VideoSlider videos={videos}/>
    </div>
  );
};

export default MediaGallery;
