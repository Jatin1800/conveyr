import styles from "./videoSection.module.css"
import AppVideo from "@/assets/conveyrVideo.mp4"

const VideoSection = () => {
  return (
    <div className={styles.videoSection}>
        <div className={styles.text}>
        <h3>About the App</h3>
          <p>Conveyr is your all-in-one platform for staying connected, expressing yourself, and engaging with loved ones. With C-chime, make your posts shine with photos, GIFs, and videos, and voice your opinions on trending topics through likes, comments, and blogs. C-Blog lets you contribute your thoughts to valuable discussions, while C-Chat allows effortless communication through video chats and voice calls. Rest assured, all your messages are end-to-end encrypted for privacy. Stay tuned for updates on new features designed to enhance your experience. Conveyr: where connection meets expression.</p>
      </div>
        <video src={AppVideo} controls></video>
    </div>
  )
}

export default VideoSection