import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <>
      <div className="flex items-center justify-between p-6 text-xl">
        <span className="flex gap-12">
          <Link to="/privacy-policy" > Privacy Policy</Link>
          <Link to="/terms-and-conditions"> Terms & Conditions</Link>
        </span>
        <span>Copyright © 2024 conveyr.com</span>
        <span>Certification area- ISO 27001 and DPDPA - coming soon</span>
        
      </div>
    </>
  );
};

export default Footer;
