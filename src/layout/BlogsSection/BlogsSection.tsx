import { useEffect, useState } from "react";
import BlogCard from "@/components/blogCard/BlogCard";
import styles from "./blogsSection.module.css";
import { wpBaseUrl, wpUserPostsDomain } from "@/lib/constants/constants";
import { Blog } from "@/types";
import { Link } from "react-router-dom";

const BlogsSection = () => {
  const [blogs, setBlogs] = useState<[Blog] | []>([]);

  async function fetchBlog() {
    const response = await fetch(`${wpBaseUrl}${wpUserPostsDomain}`,  {cache: "no-store"});
    let blogs = await response.json();
    if (blogs?.length >= 4) {
      setBlogs(blogs?.splice(0, 4));
    } else {
      setBlogs(blogs);
    }
  }
  useEffect(() => {
    fetchBlog();
  }, []);

  return (
    <div id="blog" className={styles.blogsSection}>
      <div className={styles.blogsSectionTop}>
        <h4>New blog for you</h4>
        {blogs.length > 0 && (
          <Link to="/all-blogs">
            {" "}
            <div className={styles.blogsSectionViewAll}>
              <p>View all blogs</p>{" "}
              <span className="material-symbols-outlined">east</span>
            </div>
          </Link>
        )}
      </div>
      <div className={styles.blogsSectionCards}>
        {blogs.length > 0 ? (
          blogs.map((blog) => (
            <BlogCard
              key={blog.id}
              // img={{ mediaId: blog.featured_media, type: "thumbnail" }} //send image type as large, thumbnail, medium, medium_large,
              img={{ mediaId: blog.featured_media, type: "thumbnail" }} //send image type as large, thumbnail, medium, medium_large,
              title={blog?.title?.rendered}
              desc={blog.excerpt.rendered}
              // category={Object.keys(blog.categories)}
              blogLInk={blog.slug}
              id={blog.id}
            />
          ))
        ) : (
          <h4 className="text-5xl text-[#8d4ce5] font-medium mx-[auto] mb-12">
            No Blogs Found!!
          </h4>
        )}
      </div>
    </div>
  );
};

export default BlogsSection;
