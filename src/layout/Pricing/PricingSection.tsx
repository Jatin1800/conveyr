import styles from "./pricing.module.css"
import PricingCard from "@/assets/images/pricing-card.png"

const PricingSection = () => {
  return (
    <div id="pricing" className={styles.pricing}>
        <div className={styles.pricingText}>
            <h3>Pricing Plan</h3>
            <h5>Get Awesome Features, Without Extra Charges</h5>
            <p>The four main functions of our app include making friends, deciding business goals and the methods to achieve them; organizing, best allocation of people and resources.</p>
        </div>
        <div className={styles.pricingCards}>
            <img src={PricingCard} alt="" />
        </div>
    </div>
  )
}

export default PricingSection