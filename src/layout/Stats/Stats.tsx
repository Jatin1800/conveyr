import styles from "./stats.module.css";
import MakeInIndia from "@/assets/images/make-in-india.png";

const Stats = () => {
  return (
    <div className={styles.stats}>
      <div className={styles.statsNumbers}>
        <div className="flex w-[50%] gap-8 items-center">
          <div className={styles.statsData}>
            <span>17+</span>
            <p>Star customers</p>
          </div>
          <div className={styles.statsData}>
            <span>50+</span>
            <p>App Download</p>
          </div>
        </div>
        <div className="flex w-[50%] items-center">
          <div className={styles.statsData}>
            <span>1</span>
            <p>Certified App</p>
          </div>

          <img
            src={MakeInIndia}
            alt="Make in India Logo"
            className={styles.statsMakeInIndia}
          />
        </div>
      </div>
      <div className={styles.statsText}>
        <div>
          <h2>Made In India Start up Project</h2>
          <p>
          At Start Up India, we believe in the power of ideas to transform the world. Whether you're a budding entrepreneur or a seasoned innovator, we provide the resources, guidance, and ecosystem you need to turn your vision into reality.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Stats;
