import { featuresList } from "@/lib/constants/featuresList";
import styles from "./features.module.css";
import Iphone from "@/assets/images/iPhone 15 Pro.png";

const Features = () => {
  return (
    <div id="features" className={styles.features}>
      <div className={styles.featuresImage}>
        <div className={styles.featuresImageText}>
          <h2>Application Features</h2>
          <p>We Like To Find Simple Solutions To Complex Challenges</p>
        </div>
        <img src={Iphone} alt="Iphone Image" />
      </div>
      <div className={styles.featuresList}>
        <div className="flex flex-col gap-12">
            {
                featuresList.map(item=>(
                    <div key={item.icon} className={styles.featuresListItem}>
                        <div className={styles.featuresListItemImg}>
                            <img src={`./icons/${item.icon}.png`} alt={item.icon} />
                        </div>
                        <div className={styles.featuresListItemTText}>
                            <h3>{item.title}</h3>
                            <p>{item.desc}</p>
                        </div>
                    </div>
                ))
            }
        </div>
      </div>
    </div>
  );
};

export default Features;
