import { Button } from "@/components/ui/button";
import styles from "./newsletter.module.css";
import { Input } from "@/components/ui/input";
import { useRef, useState } from "react";
import { newsletterFormID } from "@/lib/constants/constants";

const Newsletter = () => {
  const [email, setEmail] = useState('');
  const newsletterInput:any = useRef()
  // const [state, handleSubmit] = useForm(newsletterFormID);
  // const thankYouUrl:string = `https://formspree.io/${state.result?.next}`
  
  const handleSubmit = async (event:any) => {
    event.preventDefault();

    // Send email to Formspree endpoint
    let response:any = await fetch(`https://formspree.io/f/${newsletterFormID}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ 
        email,
        _subject: 'New newsletter subscription', // Custom email subject
        _body: `A new user with email address ${email} has subscribed to the newsletter.` // Custom email body
      })
    });
    response = await response.json()
    console.log("response == ", response);
    
    if (response.ok) {
      newsletterInput.current.reset()
      setEmail("")
    window.location.replace(`https://formspree.io/${response?.next}`)
      
    }
  };
   
  
  return (
    <div className={styles.newsletter}>
      <h3>
        Subscribe to our <span style={{ color: "#000" }}>Newsletter</span>
      </h3>
      <p>
        We’ll keep you in the loop on our best advice and strategies for social
        media marketing and growing a small business.
      </p>
      <form className={styles.newsletterInput} ref={newsletterInput} onSubmit={(e)=>handleSubmit(e)}>
     
        <Input
          type="email"
          placeholder="Email"
          className="rounded-full h-16 text-xl p-8"
          value={email} 
        onChange={(e) => setEmail(e.target.value)} 
        />
        <Button
          className="h-16 rounded-full text-2xl p-8 bg-[#8D4CE5]
" type="submit"
        >
          Subscribe{" "}
          <span className="material-symbols-outlined text-xl ml-1">
            north_east
          </span>
        </Button>
    
        </form>
    </div>
  );
};

export default Newsletter;
