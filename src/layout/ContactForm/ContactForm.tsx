import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";
import styles from "./contactForm.module.css";
import ContactImg from "@/assets/images/contact form.png";
import { Button } from "@/components/ui/button";
import { ValidationError, useForm } from '@formspree/react';
import { formspreeFormID } from "@/lib/constants/constants";
import { useRef } from "react";

const ContactForm = () => {
  const contactForm:any = useRef()
  const [state, handleSubmit] = useForm(formspreeFormID);
  const thankYouUrl:string = `https://formspree.io/${state.result?.next}`
  
   if(state.succeeded) { 
    contactForm.current.reset()
    window.location.replace(thankYouUrl)
  }
  

  return (
    <div id="contact" className={styles.contact}>
      <div className={styles.contactHeader}>
        <h3>
          Get in <span>touch</span>
        </h3>
        <p>Reach out, and let's create a universe of possibilities together!</p>
      </div>
      <div className={styles.contactForm}>
        <div className={styles.contactFormSub}>
          <div className={styles.contactFormSubText}>
            <h4>Let's connect</h4>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
            </p>
          </div>
          <div className={styles.contactFormSubForm}>
            <form ref={contactForm} onSubmit={handleSubmit}>
              <div className={styles.contactFormSubName}>
                <Input type="text" placeholder="First Name" name="firstName" required/>
                <Input type="text" placeholder="Last Name" name="lastName" required/>
              </div>
              <div className={styles.contactFormSubEmail}>
                <Input type="email" placeholder="Email" name="email" required />
              <ValidationError className="text-red-500 text-2xl" field="email" prefix="Email" errors={state.errors} />
                <Input type="tel" placeholder="Phone Number" name="phone" required/>
                <Textarea placeholder="Type your message here." name="message" />
                <Button type="submit" disabled={state.submitting}>
                  Send <span className="material-symbols-outlined">send</span>
                </Button>
              </div>
            </form>
          </div>
        </div>
        <div className={styles.contactFormImg}>
          <img src={ContactImg} alt="" />
        </div>
      </div>
    </div>
  );
};

export default ContactForm;
