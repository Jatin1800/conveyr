import Startup from "@/assets/images/startup-india.png"
import AppleBadge from "@/assets/images/apple-badge.png"
import GoogleBadge from "@/assets/images/google-play-badge.png"
import HeroImage from "@/assets/images/Image.png"
import styles from "./hero.module.css"
import { Link } from "react-router-dom"
import { appleStoreUrl, googleStoreUrl } from "@/lib/constants/constants"

const Hero = () => {
  return (
    <div id="home" className={styles.hero}>
        {/* Text Section */}
      <div className="flex gap-8 flex-col items-center justify-start px-[9rem] w-full md:w-1/2 md:top-[10.3rem] relative">
        <div className="flex gap-4 flex-col">
        <h1 className={styles.heroHeading}>Social media that uplifts </h1>
        <p className={styles.heroDesc}>
          Algebracorp`s – Introduces a social media application : Conveyr is
          where you connect with your people, voice has a platform, and you
          interact with your loved ones daily 24*7*365. Express yourself.
        </p>
        </div>
        <div className="w-full flex flex-wrap lg:flex-nowrap items-center justify-center gap-16 md:gap-4 mt-8 ">
            <Link to={appleStoreUrl}><img src={AppleBadge} alt="Download from Apple Store" /></Link>
            <Link to={googleStoreUrl}><img src={GoogleBadge} alt="Download from Google Store" /></Link>
        </div>
        
            <img src={Startup} alt="startup India" className="h-[9rem]" />
        
      </div>
      {/* Image Section */}
      <div id="app-download" className={styles.heroImage}>
        <img src={HeroImage} alt="" />
      </div>
    </div>
  );
};

export default Hero;
