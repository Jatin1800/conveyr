export interface Author {
  ID: number;
  URL: string;
  avatar_URL: string;
  email: boolean | string;
  first_name: string;
  ip_address: boolean | string;
  last_name: string;
  login: string;
  name: string;
  nice_name: string;
  profile_URL: string;
  site_ID: number;
  site_visible: boolean;
}
export interface Category {
    ID: number,
    name: string,
    slug: string,
    description: string,
    post_count: number,
    feed_url: string,
    parent: number,
}
export interface Attachment{
    ID: number,
    URL: string,
    guid: string,
    mime_type: string,
    width: number,
    height: number
}
export interface Blog {
    
        id: number,
        author: Author,
        date: Date,
        modified: Date,
        title: {rendered:string},
        URL: string,
        short_URL: string,
        content: {rendered:string},
        guid: string,
        status: string,
        comment_count: number,
        like_count: number,
        featured_image: string,
        categories: Category,
        attachments:{[key:number]:Attachment},
        metadata: boolean,
        excerpt:{rendered:string},
        post_thumbnail:string | null,
        slug:string,
        featured_media:number,
        tags:[]
      
}
