import { Button } from "../ui/button";
import styles from "./blogCard.module.css";
import { Link } from "react-router-dom";
import imageNotAvailable from "@/assets/images/no-image-available.webp";
import parse from "html-react-parser";
import { useEffect, useState } from "react";
import { fetchImage } from "@/lib/utils";

const BlogCard = ({
  id,
  img,
  title,
  category,
  desc,
  blogLInk = "/",
  className = "",
}: any) => {
  const [imgUrl, setImgUrl] = useState<string>("");

  useEffect(() => {
    (async () => {
      const url = await fetchImage(img.mediaId, img.type);
      setImgUrl(url);
    })();
  }, []);
  return (
    <div className={`${styles.blogCard} ${className}`}>
      <div className={styles.blogCardImg}>
        <img src={img ? imgUrl : imageNotAvailable} alt="" />
      </div>
      <div className={styles.blogCardText}>
        <div className={styles.blogCardTextCategory}>
          {category?.map((item: string) => (
            <Button key={item} variant="outline" className="rounded-3xl">
              {item}
            </Button>
          ))}
        </div>
        <h2>{parse(title)}</h2>
        <div className={styles.description}>{parse(desc)}</div>
        <Link
          to={`/${blogLInk}/${id}`}
          className={styles.blogCardTextReadMoreLink}
        >
          {" "}
          <div className={styles.blogCardTextReadMore}>
            Read More <span className="material-symbols-outlined">east</span>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default BlogCard;
