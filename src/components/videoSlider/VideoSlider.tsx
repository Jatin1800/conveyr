import styles from "./videoSlider.module.css";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

type Params = {
  videos: any[];
};

const VideoSlider = ({ videos }: Params) => {
  console.log("videos === ", videos)
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    vertical: false,
  };
  return (
    <div className={styles.video_slider}>
      <Slider {...settings}>
        {videos.length>0 && videos.map((video) => (
          <div key={video.id} className={styles.video_item}>
            <video controls>
              <source src={video.source_url} type={video.mime_type} />
              Your browser does not support the video tag.
            </video>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default VideoSlider;
